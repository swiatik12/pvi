const addDeleteStudentActionToButton = (button) => {
	button.addEventListener("click", () => {
		const row = button.closest("tr");
		const fullName = row.querySelector('td.cell-name')

		const warningModal = document.querySelector(".warning-modal");
		const warningContent = document.querySelector(".warning-modal .modal-body");
		const approveButton = document.querySelector(".warning-modal .approve");
		const cancelButton = document.querySelector(".warning-modal .cancel");
		const closeButton = document.querySelector(".warning-modal .close");


		//warningContent.textContent += fullName.textContent;
		warningModal.classList.add("show-modal");

		approveButton.addEventListener("click", () => {
			row.remove();
			warningModal.classList.remove("show-modal");
			//warningContent.textContent = warningContent.textContent.replace(fullName.textContent, '')

		});

		cancelButton.addEventListener("click", () => {
			warningModal.classList.remove("show-modal");
			//warningContent.textContent = warningContent.textContent.replace(fullName.textContent, '')

		});
		closeButton.addEventListener("click", () => {
			warningModal.classList.remove("show-modal");
			//warningContent.textContent = warningContent.textContent.replace(fullName.textContent, '')

		});


	});
}

const addStudentsActions = () => {
	const deleteButtons = document.querySelectorAll(".delete-btn");
	deleteButtons.forEach((button) => {
		addDeleteStudentActionToButton(button)
	});
}

const addAnimationToNotification = () => {
	const notification = document.querySelector('.notification-logo ')
	notification.addEventListener('dblclick', () => {
		notification.classList.remove('bell-animation')
		notification.offsetWidth
		notification.classList.add('bell-animation')
	})
}

const addStudent = () => {
	const addButton = document.querySelector(".add-student-btn");
	const modal = document.querySelector(".edit-modal");
	const closeModalBtn = modal.querySelector(".close");
	const createStudentBtn = modal.querySelector(".create");

	addButton.addEventListener("click", () => (modal.classList.add('show-modal')));
	closeModalBtn.addEventListener("click", () => (modal.classList.remove('show-modal')));
	createStudentBtn.addEventListener("click", () => {
		const generateNewStudent = (status, group, name, gender, birthday) => {
			return `		
			<td class="cell-checkbox">
				<label class="container">
					<input type="checkbox" ${status === "active" && "checked"} />
					<span class="checkmark"></span>
				</label>
			</td>
			<td>${group}</td>
			<td class="cell-name">${name}</td>
			<td>${gender}</td>
			<td>${birthday}</td>
			<td><span class="cell-status ${status === "active" ? "active-status" : "non-active"
				} active-status"></span></td>
				<td class="cell-options">
				<button><img src="d:\\Users\\User\\Downloads\\pencil.svg" alt="edit" /></button
				><button class="delete-btn">
					<img src="d:\\Users\\User\\Downloads\\trash.svg" alt="delete" />
				</button>
			</td>
		`;
		};

		const table = document.querySelector(".table");
		const row = table.insertRow();
		row.innerHTML = generateNewStudent('active', 'PZ-25', 'Sviatoslav Chaikovskyi', 'M', '09.05.2004');
		const button = row.querySelector('.delete-btn')
		modal.classList.remove('show-modal')
		addDeleteStudentActionToButton(button)
	});
};


const bootstrap = () => {
	addStudent();
	addStudentsActions()
	addAnimationToNotification()
};

bootstrap();
